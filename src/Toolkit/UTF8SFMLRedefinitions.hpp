#pragma once

#include <SFML/Audio.hpp>
#include <SFML/Audio/InputSoundFile.hpp>
#include <SFML/Audio/SoundBuffer.hpp>
#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Shader.hpp>
#include <SFML/Graphics/Texture.hpp>

#include "UTF8SFML.hpp"

namespace jujube {
    using Music = jujube::UTF8Streamer<sf::Music>;
    using InputSoundFile = jujube::UTF8Streamer<sf::InputSoundFile>;
    using SoundBuffer = jujube::UTF8Loader<sf::SoundBuffer>;
    using Texture = jujube::UTF8Loader<sf::Texture>;
    using Shader = jujube::UTF8Loader<sf::Shader>;
    using Font = jujube::UTF8StreamerUsingLoadFrom<sf::Font>;
}