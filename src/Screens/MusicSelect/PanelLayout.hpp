#pragma once

#include <array>
#include <functional>
#include <memory>
#include <map>

#include "../../Data/Song.hpp"
#include "Resources.hpp"
#include "Panels/Panel.hpp"

namespace MusicSelect {

    class Panel;

    // PanelLayout restricts the ways you can create a scrollable grid of panels usable in a Ribbon
    class PanelLayout : public std::vector<std::array<std::shared_ptr<Panel>,3>> {
    public:
        using PanelPtr = std::shared_ptr<Panel>;

        // Takes of multimap of category name and associated Panels, useful for all the sorted layouts
        template<typename T>
        explicit PanelLayout(const T& categories, ScreenResources& t_resources) {
            std::optional<typename T::key_type> current_category;
            std::vector<std::shared_ptr<Panel>> current_column;

            for (auto &&[category, panel] : categories) {

                // Handle category change
                if (category != current_category) {
                    if (not current_column.empty()) {
                        add_column(current_column, t_resources);
                        current_column.clear();
                    }

                    // Add new category
                    current_column.emplace_back(std::make_shared<CategoryPanel>(t_resources, category));
                    current_category = category;
                }

                // Current column is full, add it and start a new one
                if (current_column.size() == 3) {
                    push_back({current_column[0],current_column[1],current_column[2]});
                    current_column.clear();
                }

                current_column.push_back(panel);
            }

            // Handle anything left over in the current column
            if (not current_column.empty()) {
                add_column(current_column, t_resources);
            }

            fill_layout(t_resources);
        }

        // Arranges all the panels in the vector in columns of three
        explicit PanelLayout(const std::vector<std::shared_ptr<Panel>>& panels, ScreenResources& t_resources);

        // Stepmania-like empty layout with big red panels that say EMPTY
        static PanelLayout red_empty_layout(ScreenResources& t_resources);

        // Standard title sort with categories for each letter
        static PanelLayout title_sort(const Data::SongList& song_list, ScreenResources& t_resources);

        // Difficulty sort with categories for each level
        static PanelLayout level_sort(const Data::SongList& song_list, ScreenResources& t_resources);

    private:
        void fill_layout(ScreenResources& t_resources);
        void add_column(const std::vector<std::shared_ptr<Panel>>& column, ScreenResources& t_resources);
    };
}