#include "PanelLayout.hpp"

namespace {
    using SongPtr = std::shared_ptr<const Data::Song>;
    using SongDiff = std::tuple<unsigned int, std::string, SongPtr>;

    bool OrderBySongTitle(const SongPtr& a, const SongPtr& b) {
        return Data::Song::sort_by_title(*a, *b);
    }

    bool OrderBySongDiff(const SongDiff& a, const SongDiff& b) {
        if (std::get<0>(a) != std::get<0>(b)) {
            return std::get<0>(a) < std::get<0>(b);
        }

        return OrderBySongTitle(std::get<2>(a), std::get<2>(b));
    }

    std::string TitleCategory(const SongPtr& song) {
        if (song->title.size() > 0) {
            char letter = song->title[0];

            if ('A' <= letter and letter <= 'Z') {
                return std::string(1, letter);
            } else if ('a' <= letter and letter <= 'z') {
                return std::string(1, 'A' + (letter - 'a'));
            }
        }

        return "?";
    }
}
namespace MusicSelect {
    PanelLayout::PanelLayout(
        const std::vector<std::shared_ptr<Panel>>& panels,
        ScreenResources& t_resources
    ) {
        std::vector<std::shared_ptr<Panel>> current_column;
        for (auto& panel : panels) {
            if (current_column.size() == 3) {
                push_back({current_column[0],current_column[1],current_column[2]});
                current_column.clear();
            }
            current_column.push_back(panel);
        }
        if (not current_column.empty()) {
            while (current_column.size() < 3) {
                current_column.emplace_back(std::make_shared<EmptyPanel>(t_resources));
            }
            push_back({current_column[0],current_column[1],current_column[2]});
        }
        fill_layout(t_resources);
    }

    PanelLayout PanelLayout::red_empty_layout(ScreenResources& t_resources) {
        std::vector<std::shared_ptr<Panel>> panels;
        for (size_t i = 0; i < 3*4; i++) {
            panels.emplace_back(std::make_shared<ColoredMessagePanel>(t_resources, sf::Color::Red, "- EMPTY -"));
        }
        return PanelLayout{panels, t_resources};
    }

    PanelLayout PanelLayout::title_sort(const Data::SongList& song_list, ScreenResources& t_resources) {

        std::vector<SongPtr> tmp_songs;
        tmp_songs.reserve(song_list.songs.size());
        tmp_songs.assign(song_list.songs.begin(), song_list.songs.end());
        std::sort(tmp_songs.begin(), tmp_songs.end(), OrderBySongTitle);

        std::multimap<std::string, PanelPtr> categories;

        for (const auto &song : tmp_songs) {
            std::string category = TitleCategory(song);

            categories.emplace(std::make_pair(category, std::make_shared<SongPanel>(t_resources, song)));
        }

        return PanelLayout{categories, t_resources};
    }

    PanelLayout PanelLayout::level_sort(const Data::SongList& song_list, ScreenResources& t_resources) {
        std::vector<SongDiff> song_diffs;

        // Reserve 3x the number of songs for the number of difficulties
        song_diffs.reserve(song_list.songs.size()*3);

        for (const auto& song : song_list.songs) {
            for (const auto& [diff, level] : song->chart_levels) {
                song_diffs.emplace_back(std::make_tuple(level, diff, song));
            }
        }

        std::sort(song_diffs.begin(), song_diffs.end(), OrderBySongDiff);

        std::multimap<unsigned int, PanelPtr> categories;

        for (const auto& [level, diff, song] : song_diffs) {
            // NOTE: The extra parameter on the SongPanel constructor, this locks the difficulty for that panel
            categories.emplace(std::make_pair(level, std::make_shared<SongPanel>(t_resources, song, diff)));
        }

        return PanelLayout{categories, t_resources};
    }

    void PanelLayout::fill_layout(ScreenResources& t_resources) {
        while (size() < 4) {
            push_back({
                std::make_shared<EmptyPanel>(t_resources),
                std::make_shared<EmptyPanel>(t_resources),
                std::make_shared<EmptyPanel>(t_resources)
            });
        }
    }

    void PanelLayout::add_column(const std::vector<std::shared_ptr<Panel>>& column, ScreenResources& t_resources) {
        if (column.size() == 3) {
            push_back({column[0],column[1],column[2]});
        } else if (column.size() == 2) {
            push_back({column[0],column[1],std::make_shared<EmptyPanel>(t_resources)});
        } else if (column.size() == 1) {
            push_back({column[0],std::make_shared<EmptyPanel>(t_resources),std::make_shared<EmptyPanel>(t_resources)});
        } else {
            push_back({
                std::make_shared<EmptyPanel>(t_resources),
                std::make_shared<EmptyPanel>(t_resources),
                std::make_shared<EmptyPanel>(t_resources)
            });
        }
    }
}
